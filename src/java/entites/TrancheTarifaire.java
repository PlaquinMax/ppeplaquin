/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entites;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pierre
 */
@Entity
@XmlRootElement
public class TrancheTarifaire implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long idTranche;
    private Float qteDebutTranche;
    private Float qteFinTranche;
    private Float prixUnitaireTranche;
    
    @JoinColumn(name="REFPROD")
    @ManyToOne
    private Produit leProduit;

    public TrancheTarifaire() {
    }

    public TrancheTarifaire(Long idTranche, Float qteDebutTranche, Float qteFinTranche, Float prixUnitaireTranche, Produit leProduit) {
        this.idTranche = idTranche;
        this.qteDebutTranche = qteDebutTranche;
        this.qteFinTranche = qteFinTranche;
        this.prixUnitaireTranche = prixUnitaireTranche;
        this.leProduit = leProduit;
    }

    public Long getIdTranche() {
        return idTranche;
    }

    public void setIdTranche(Long idTranche) {
        this.idTranche = idTranche;
    }

    public Float getQteDebutTranche() {
        return qteDebutTranche;
    }

    public void setQteDebutTranche(Float qteDebutTranche) {
        this.qteDebutTranche = qteDebutTranche;
    }

    public Float getQteFinTranche() {
        return qteFinTranche;
    }

    public void setQteFinTranche(Float qteFinTranche) {
        this.qteFinTranche = qteFinTranche;
    }

    public Float getPrixUnitaireTranche() {
        return prixUnitaireTranche;
    }

    public void setPrixUnitaireTranche(Float prixUnitaireTranche) {
        this.prixUnitaireTranche = prixUnitaireTranche;
    }

    public Produit getLeProduit() {
        return leProduit;
    }

    public void setLeProduit(Produit leProduit) {
        this.leProduit = leProduit;
    }
}
