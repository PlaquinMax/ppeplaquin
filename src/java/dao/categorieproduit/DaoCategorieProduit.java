package dao.categorieproduit;

import entites.CategorieProduit;
import java.util.List;

public interface DaoCategorieProduit
{
    List<CategorieProduit> getToutesLesCategoriesProduit();
    CategorieProduit getCategorieProduit(String pNumCateg);
}
