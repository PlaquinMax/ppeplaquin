package dao.categorieproduit;

import entites.CategorieProduit;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class DaoCategorieProduitImpl implements DaoCategorieProduit, Serializable
{
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public List<CategorieProduit> getToutesLesCategoriesProduit()
    {
        return em.createQuery("Select c from CategorieProduit c").getResultList();
    }

    @Override
    public CategorieProduit getCategorieProduit(String pNumCateg)
    {
        return em.find(CategorieProduit.class, pNumCateg);
    }
    
}
