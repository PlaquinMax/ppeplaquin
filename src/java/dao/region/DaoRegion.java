
package dao.region;

import entites.Region;
import java.util.List;

public interface DaoRegion {
    Region getRegion(String pRegion);
    List<Region>  getToutesLesRegions();
}
