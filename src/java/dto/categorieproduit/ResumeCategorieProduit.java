package dto.categorieproduit;

public class ResumeCategorieProduit
{
    private String codeCateg;
    private String nomCateg;
    private Float caAnnuel;
    
    public ResumeCategorieProduit(){}
    public ResumeCategorieProduit(String codeCateg, String nomCateg, Float caAnnuel) {
        this.codeCateg = codeCateg;
        this.nomCateg = nomCateg;
        this.caAnnuel = caAnnuel;
    }
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS / SETTERS">
    
    public String getCodeCateg() {
        return codeCateg;
    }
    
    public void setCodeCateg(String codeCateg) {
        this.codeCateg = codeCateg;
    }
    
    public String getNomCateg() {
        return nomCateg;
    }
    
    public void setNomCateg(String nomCateg) {
        this.nomCateg = nomCateg;
    }
    public Float getCaAnnuel() {
        return caAnnuel;
    }

    public void setCaAnnuel(Float caAnnuel) {
        this.caAnnuel = caAnnuel;
    }
    
    
//</editor-fold>

    
}
