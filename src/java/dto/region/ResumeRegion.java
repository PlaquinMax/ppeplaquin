package dto.region;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Théo
 */
@XmlRootElement
public class ResumeRegion
{
    private String codeRegion;
    private String nomRegion;
    private Float caAnnuel;
    private int nbClients;
    
    public ResumeRegion(){}
    public ResumeRegion(String codeRegion, String nomRegion, Float caAnnuel, int nbClients) {
        this.codeRegion = codeRegion;
        this.nomRegion = nomRegion;
        this.caAnnuel = caAnnuel;
        this.nbClients = nbClients;
    }

    public String getCodeRegion() {
        return codeRegion;
    }
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS / SETTERS">
    
    public void setCodeRegion(String codeRegion) {
        this.codeRegion = codeRegion;
    }
    
    public String getNomRegion() {
        return nomRegion;
    }
    
    public void setNomRegion(String nomRegion) {
        this.nomRegion = nomRegion;
    }
    
    public Float getCaAnnuel() {
        return caAnnuel;
    }
    
    public void setCaAnnuel(Float caAnnuel) {
        this.caAnnuel = caAnnuel;
    }
    
    public int getNbClients() {
        return nbClients;
    }
    
    public void setNbClients(int nbClients) {
        this.nbClients = nbClients;
    }
    
//</editor-fold>
}
