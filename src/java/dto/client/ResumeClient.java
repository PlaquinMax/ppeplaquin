package dto.client;

import entites.Commande;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Théo
 */
@XmlRootElement
public class ResumeClient
{
    private Long numClient;
    private String nomClient;
    private String adrClient;
    private String email;
    private String password;
    
    private List<Commande> lesCommandes;
    
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS / SETTERS">
    
    public ResumeClient(){}
    
    public Long getNumClient() {
        return numClient;
    }
    
    public void setNumClient(Long numClient) {
        this.numClient = numClient;
    }
    
    public String getNomClient() {
        return nomClient;
    }
    
    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }
    
    public String getAdrClient() {
        return adrClient;
    }
    
    public void setAdrClient(String adrClient) {
        this.adrClient = adrClient;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    public List<Commande> getLesCommandes() {
        return lesCommandes;
    }

    public void setLesCommandes(List<Commande> lesCommandes) {
        this.lesCommandes = lesCommandes;
    }
//</editor-fold>
}
