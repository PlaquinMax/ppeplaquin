package webservice.arbres.categorieproduit;

import entites.CategorieProduit;
import java.io.Serializable;
import dao.Dao;
import entites.LigneDeCommande;
import entites.Produit;
import javax.inject.Inject;

public class ArbresCategorieProduitImpl implements ArbresCategorieProduit, Serializable
{
    @Inject Dao         dao;
    
    @Override
    public void creerArbreCategorieProduit(CategorieProduit categProduit)
    {
        dao.detache(categProduit);
        for(Produit p : categProduit.getLesProduits())
        {
            p.setLaCategorie(null);
            for(LigneDeCommande lgdc : p.getLesLignesDeCommande())
            {
                lgdc.setLeProduit(null);
            }
        }
    }
}
