package webservice.arbres.categorieproduit;

import entites.CategorieProduit;

public interface ArbresCategorieProduit
{
    void creerArbreCategorieProduit(CategorieProduit categProduit);
}
