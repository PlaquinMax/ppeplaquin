package webservice.arbres.commande;

import dao.Dao;
import entites.Commande;
import java.io.Serializable;
import javax.inject.Inject;

public class ArbresCommandeImpl implements ArbresCommande, Serializable
{
    @Inject Dao dao;
    
    @Override
    public void creerArbreCommande(Commande cmd)
    {
        dao.detache(cmd);
        cmd.setLesLignesDeCommande(null);
        cmd.getLeClient().setLesCommandes(null);
        cmd.getLeClient().getLaRegion().setLesClients(null);
    }
}
