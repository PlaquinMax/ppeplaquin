package webservice.arbres.commande;

import entites.Commande;

public interface ArbresCommande
{
    void creerArbreCommande(Commande cmd);
}
