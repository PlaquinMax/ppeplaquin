package webservice.soap;

import dto.categorieproduit.ResumeCategorieProduit;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.CategorieProduit;
import entites.Commande;
import java.util.List;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import webservice.arbres.commande.ArbresCommande;
import webservice.modele.ModeleWebServiceCommande;

@WebService(serviceName = "WebServiceCommande")
public class WebServiceCommande
{
    @Inject ModeleWebServiceCommande    modele;
    @Inject ArbresCommande              arbres;
    
    @WebMethod(operationName = "getCommande")
    public Commande getCommande(@WebParam(name = "pNumCom") Long pNumCom)
    {
        Commande cmd = modele.getCommance(pNumCom);
        arbres.creerArbreCommande(cmd);
        return cmd;
    }

    @WebMethod(operationName = "getToutesLesCommandes")
    public List<Commande> getToutesLesCommandes()
    {
        List<Commande> lc = modele.getToutesLesCommandes();
        
        for(Commande cmd : lc)
        {
            arbres.creerArbreCommande(cmd);
        }
        
        return lc;
    }

    @WebMethod(operationName = "montantHT")
    public Float montantHT(@WebParam(name = "pNumCom") Long pNumCom)
    {
        Commande cmd = modele.getCommance(pNumCom);
        return modele.montantHT(cmd);
    }

    @WebMethod(operationName = "montantTTC")
    public Float montantTTC(@WebParam(name = "pNumCom") Long pNumCom)
    {
        Commande cmd = modele.getCommance(pNumCom);
        return modele.montantTTC(cmd);
    }

    @WebMethod(operationName = "getResumesLigneDeCommande")
    public List<ResumeLigneDeCommande> getResumesLigneDeCommande(@WebParam(name = "pNumCom") Long pNumCom)
    {
        return modele.getResumesLigneDeCommande(pNumCom);
    }

    @WebMethod(operationName = "getToutesLesCategoriesProduit")
    public List<ResumeCategorieProduit> getToutesLesCategoriesProduit()
    {
        return modele.getResumesCategorieProduit();
    }

    @WebMethod(operationName = "getCategorieProduit")
    public CategorieProduit getCategorieProduit(@WebParam(name = "pCateg") String pCateg)
    {
        return modele.getCategorieProduit(pCateg);
    }
}