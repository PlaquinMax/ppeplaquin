package webservice.modele;

import dao.commande.DaoCommande;
import dto.commande.ResumeCommande;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.Commande;
import entites.LigneDeCommande;
import fabrique.dto.commande.FabResumesCommandes;
import fabrique.dto.lignedecommande.FabResumesLigneDeCommande;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 *
 * @author JB
 */
@Singleton
public class ModeleWebServRestCommandeImpl implements ModeleWebServRestCommande, Serializable
{
    @Inject FabResumesCommandes         fabResumesCommandes;
    @Inject FabResumesLigneDeCommande   fabResumesLgdc;
    @Inject DaoCommande                 daoCommande;
    
    @Override
    public ResumeCommande getLeResumeCommande(Long pNumCom)
    {
        return fabResumesCommandes.getResumeCommande(pNumCom);
    }

    @Override
    public List<ResumeCommande> getLesResumesCommande()
    {
        return fabResumesCommandes.getLesResumesCommande();
    }

    @Override
    public List<ResumeLigneDeCommande> getLesResumesLigneDeCommande(List<LigneDeCommande> lgdc)
    {
        return fabResumesLgdc.getResumesLigneDeCommande(lgdc);
    } 

    @Override
    public Commande getCommande(Long pNumCom)
    {
        return daoCommande.getCommande(pNumCom);
    }
}
