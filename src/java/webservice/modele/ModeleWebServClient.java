package webservice.modele;

import dto.client.ResumeClient;
import java.util.List;

/**
 *
 * @author JB
 */
public interface ModeleWebServClient
{
    ResumeClient getResumeClient(Long pNumCli);
    List<ResumeClient> getLesResumesClient();
}
