package webservice.modele;

import bal.categorieproduit.BalCategorieProduit;
import bal.commande.BalCommande;
import bal.lignedecommande.BalLigneDeCommande;
import dao.categorieproduit.DaoCategorieProduit;
import dao.commande.DaoCommande;
import dto.categorieproduit.ResumeCategorieProduit;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.CategorieProduit;
import entites.Commande;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import utilitaires.UtilDate;

public class ModeleWebServiceCommandeImpl implements ModeleWebServiceCommande, Serializable
{
    @Inject DaoCommande         daoCommande;
    @Inject BalCommande         balCommande;
    @Inject DaoCategorieProduit daoCateg;
    @Inject BalLigneDeCommande  balLgdc;
    @Inject BalCategorieProduit balCateg;
    
    @Override
    public Commande getCommance(Long pNumCom)
    {
        return daoCommande.getCommande(pNumCom);
    }

    @Override
    public List<Commande> getToutesLesCommandes()
    {
        return daoCommande.getToutesLesCommandes();
    }

    @Override
    public Float montantHT(Commande cmd)
    {
        return balCommande.montantCommandeHT(cmd);
    }

    @Override
    public Float montantTTC(Commande cmd)
    {
        return balCommande.montantCommandeTTC(cmd);
    }

    @Override
    public List<ResumeLigneDeCommande> getResumesLigneDeCommande(Long pNumCom)
    {
        Commande cmd = daoCommande.getCommande(pNumCom);
        
        return balLgdc.getResumesLigneDeCommande(cmd.getLesLignesDeCommande());
    }

    @Override
    public List<ResumeCategorieProduit> getResumesCategorieProduit()
    {
        return balCateg.getResumesCategoriesProduit();
    }

    @Override
    public CategorieProduit getCategorieProduit(String pCateg)
    {
        return daoCateg.getCategorieProduit(pCateg);
    }
}