package webservice.modele;

import dto.categorieproduit.ResumeCategorieProduit;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.CategorieProduit;
import entites.Commande;
import java.util.List;

public interface ModeleWebServiceCommande
{
    Commande getCommance(Long pNumCom);
    List<Commande> getToutesLesCommandes();
    
    Float montantHT(Commande cmd);
    Float montantTTC(Commande cmd);
    
    List<ResumeLigneDeCommande> getResumesLigneDeCommande(Long pNumCom);
    
    List<ResumeCategorieProduit> getResumesCategorieProduit();
    CategorieProduit getCategorieProduit(String pCateg);
}