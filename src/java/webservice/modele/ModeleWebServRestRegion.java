package webservice.modele;

import dto.region.ResumeRegion;
import java.util.List;

/**
 *
 * @author JB
 */
public interface ModeleWebServRestRegion
{
    ResumeRegion getResumeRegion(String pCodeRegion);
    List<ResumeRegion> getLesResumesRegions();
}
