package webservice.modele;

import dto.client.ResumeClient;
import fabrique.dto.client.FabResumeClient;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author JB
 */
public class ModeleWebServClientImpl implements ModeleWebServClient, Serializable
{
    @Inject FabResumeClient fab;
    
    @Override
    public ResumeClient getResumeClient(Long pNumCli)
    {
        return fab.getResumeClient(pNumCli);
    }

    @Override
    public List<ResumeClient> getLesResumesClient()
    {
        return fab.getLesResumesClient();
    }
}
