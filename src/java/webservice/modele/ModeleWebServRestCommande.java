package webservice.modele;

import dto.commande.ResumeCommande;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.Commande;
import entites.LigneDeCommande;
import java.util.List;

/**
 *
 * @author JB
 */
public interface ModeleWebServRestCommande
{
    Commande getCommande(Long pNumCom);
    
    ResumeCommande getLeResumeCommande(Long pNumCom);
    List<ResumeCommande> getLesResumesCommande();
    
    List<ResumeLigneDeCommande> getLesResumesLigneDeCommande(List<LigneDeCommande> lgdc);
}
