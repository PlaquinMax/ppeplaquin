package webservice.modele;

import dto.region.ResumeRegion;
import fabrique.dto.region.FabResumesRegion;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author JB
 */
public class ModeleWebServRestRegionImpl implements ModeleWebServRestRegion, Serializable
{
    @Inject FabResumesRegion    fabResumesRegion;
    
    @Override
    public ResumeRegion getResumeRegion(String pCodeRegion)
    {
        return fabResumesRegion.getResumeRegion(pCodeRegion);
    }

    @Override
    public List<ResumeRegion> getLesResumesRegions()
    {
        return fabResumesRegion.getLesResumesRegion();
    } 
}
