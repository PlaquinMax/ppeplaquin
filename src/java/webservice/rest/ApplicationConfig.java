package webservice.rest;

import java.util.Set;
import javax.ws.rs.core.Application;
import javax.ws.rs.ApplicationPath;


@ApplicationPath("rest")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
        addRestResourceClasses(resources);
        return resources;
    }

    
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(webservice.rest.client.WebServRestClient.class);
        resources.add(webservice.rest.commande.WebServRestCommande.class);
        resources.add(webservice.rest.region.WebServiceRestRegion.class);
    }
    
}
