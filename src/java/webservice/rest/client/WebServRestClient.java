package webservice.rest.client;

import dto.client.ResumeClient;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import webservice.modele.ModeleWebServClient;

@Stateless
@Path("client")
public class WebServRestClient
{
    @Inject ModeleWebServClient modele;
    
    @GET
    @Path("tous")
    @Produces({"application/xml", "application/json"})
    public List<ResumeClient> getLesClients()
    {
        return modele.getLesResumesClient();
    }
    
    @GET
    @Path("{numcli}")
    @Produces({"application/xml", "application/json"})
    public ResumeClient getClient(@PathParam("numcli")Long pNumCli)
    {
        return modele.getResumeClient(pNumCli);
    }
}
