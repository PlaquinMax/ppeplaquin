package webservice.rest.region;

import dto.region.ResumeRegion;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import webservice.modele.ModeleWebServRestRegion;

/**
 *
 * @author JB
 */
@Stateless
@Path("region")
public class WebServiceRestRegion
{
    @Inject ModeleWebServRestRegion modele;
    
    @GET
    @Path("numero/{coderegion}")
    @Produces({"application/xml", "application/json"})
    public ResumeRegion getResumeRegion(@PathParam("coderegion")String pCodeRegion)
    {
        return modele.getResumeRegion(pCodeRegion);
    }
    
    @GET
    @Path("toutes")
    @Produces({"application/xml", "application/json"})
    public List<ResumeRegion> getLesResumesRegion()
    {
        return modele.getLesResumesRegions();
    }
}
