package webservice.rest.commande;

import dto.commande.ResumeCommande;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.Commande;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import webservice.modele.ModeleWebServRestCommande;

@Stateless
@Path("commande")
public class WebServRestCommande
{
    @Inject ModeleWebServRestCommande modele;
    
    @GET
    @Path("toutes")
    @Produces({"application/xml", "application/json"})
    public List<ResumeCommande> getLesCommandes()
    {
        return modele.getLesResumesCommande();
    }
    
    @GET
    @Path("numero/{numcom}")
    @Produces({"application/xml", "application/json"})
    public ResumeCommande getCommande(@PathParam("numcom")Long pNumCom)
    {
        return modele.getLeResumeCommande(pNumCom);
    }
    
    @GET
    @Path("lignesdecommande/{numcom}")
    @Produces({"application/xml", "application/json"})
    public List<ResumeLigneDeCommande> getLesLignesDeCommandes(@PathParam("numcom")Long pNumCom)
    {
        Commande cmd = modele.getCommande(pNumCom);
        return modele.getLesResumesLigneDeCommande(cmd.getLesLignesDeCommande());
    }
}
