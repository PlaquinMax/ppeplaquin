package bal.commande;

import dao.commande.DaoCommande;
import dao.tva.DaoTva;
import bal.lignedecommande.*;
import entites.Commande;
import entites.LigneDeCommande;
import entites.TrancheTarifaire;
import java.io.Serializable;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class BalCommandeImpl implements BalCommande, Serializable {

    @Inject
    DaoTva daoTva;
    @Inject   BalLigneDeCommande balLigneDeCommande;
    @Override
    public Float montantCommandeHT(Commande pCommande) {

        Float montant = 0F;
        for (LigneDeCommande lgdc : pCommande.getLesLignesDeCommande()) {
            montant+=balLigneDeCommande.caLigneDeCommandeHT(lgdc);

        }
        return montant;
    }

    @Override
    public Float montantCommandeTTC(Commande pCommande) {

        Float montant = 0F;
        for (LigneDeCommande lgdc : pCommande.getLesLignesDeCommande()) {
            montant+=balLigneDeCommande.caLigneDeCommandeTTC(lgdc);
            
        }

        return montant;
    }

    @Override
    public boolean estReglee(Commande pCommande) {
        return pCommande.getEtatCom().equalsIgnoreCase("R");
    }
}
