package bal.categorieproduit;

import dto.categorieproduit.ResumeCategorieProduit;
import entites.CategorieProduit;
import java.util.List;

public interface BalCategorieProduit
{
    Float caAnnuelCategorieProduit(CategorieProduit pCateg, int annee);
    List<ResumeCategorieProduit> getResumesCategoriesProduit();
}
