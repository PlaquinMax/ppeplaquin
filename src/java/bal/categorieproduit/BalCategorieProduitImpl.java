package bal.categorieproduit;

import bal.produit.BalProduit;
import dao.categorieproduit.DaoCategorieProduit;
import dto.categorieproduit.ResumeCategorieProduit;
import entites.CategorieProduit;
import entites.Produit;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import utilitaires.UtilDate;

public class BalCategorieProduitImpl implements BalCategorieProduit, Serializable
{
    @Inject BalProduit          balProduit;
    @Inject DaoCategorieProduit daoCateg;
    
    @Override
    public Float caAnnuelCategorieProduit(CategorieProduit pCateg, int annee)
    {
        Float ca = 0F;
        
        for(Produit produit : pCateg.getLesProduits())
        {
            ca += balProduit.caAnnuelProduit(produit, annee);
        }
        
        return ca;
    }
    
    private ResumeCategorieProduit creerResumeCategorieProduit(CategorieProduit categProduit)
    {
        return new ResumeCategorieProduit(categProduit.getCodeCateg(), categProduit.getNomCateg(), caAnnuelCategorieProduit(categProduit, UtilDate.anneeCourante()));
    }

    @Override
    public List<ResumeCategorieProduit> getResumesCategoriesProduit()
    {
        List<ResumeCategorieProduit> lc = new LinkedList();
        
        for(CategorieProduit categ : daoCateg.getToutesLesCategoriesProduit())
        {
            lc.add(creerResumeCategorieProduit(categ));
        }
        
        return lc;
    }
}
