package bal.lignedecommande;

import dto.lignedecommande.ResumeLigneDeCommande;
import entites.LigneDeCommande;
import java.util.List;

public interface BalLigneDeCommande
{
    Float caLigneDeCommandeHT(LigneDeCommande lgdc);
    Float caLigneDeCommandeTTC(LigneDeCommande lgdc);
    Float prixUnitaireTranche(LigneDeCommande lgdc);
    
    List<ResumeLigneDeCommande> getResumesLigneDeCommande(List<LigneDeCommande> lgdc);
}
