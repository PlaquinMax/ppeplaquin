package bal.lignedecommande;

import dto.lignedecommande.ResumeLigneDeCommande;
import entites.LigneDeCommande;
import entites.Produit;
import entites.TrancheTarifaire;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Singleton;

@Singleton
public class BalLigneDeCommandeImpl implements BalLigneDeCommande, Serializable
{
    @Override
    public Float prixUnitaireTranche(LigneDeCommande lgdc)
    {
        Float montant = 0F;
        if(!lgdc.getLeProduit().getLesTranchesTarifaires().isEmpty())
        {
            for(TrancheTarifaire tt : lgdc.getLeProduit().getLesTranchesTarifaires())
            {
                if(lgdc.getQteCom() <= tt.getQteFinTranche() && lgdc.getQteCom() >= tt.getQteDebutTranche())
                {
                    montant = lgdc.getLeProduit().getPrixProd() * tt.getPrixUnitaireTranche() / 100;
                }
            }
            return montant;
        }
        else
        {
            return lgdc.getLeProduit().getPrixProd();
        }
    }

    @Override
    public Float caLigneDeCommandeHT(LigneDeCommande lgdc)
    {

        return prixUnitaireTranche(lgdc) * lgdc.getQteCom();
    }

    @Override
    public Float caLigneDeCommandeTTC(LigneDeCommande lgdc)
    {
        return caLigneDeCommandeHT(lgdc) * (1 + lgdc.getLeProduit().getLaCategorie().getTauxTva());
    }
    
    private ResumeLigneDeCommande creerResumeLigneDeCommande(LigneDeCommande lgdc)
    {
        Produit p = lgdc.getLeProduit();
        
        return new ResumeLigneDeCommande(p.getRefProd(), p.getDesigProd(), p.getPrixProd(), lgdc.getQteCom(), caLigneDeCommandeHT(lgdc), caLigneDeCommandeTTC(lgdc));
    }

    @Override
    public List<ResumeLigneDeCommande> getResumesLigneDeCommande(List<LigneDeCommande> lgdc)
    {
        List<ResumeLigneDeCommande> lc = new LinkedList();
        
        for(LigneDeCommande l : lgdc)
        {
            lc.add(creerResumeLigneDeCommande(l));
        }
        
        return lc;
    }
}
