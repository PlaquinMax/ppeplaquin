package web.controleurs;

import bal.categorieproduit.BalCategorieProduit;
import dao.categorieproduit.DaoCategorieProduit;
import entites.CategorieProduit;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.chart.PieChartModel;
import utilitaires.UtilDate;

@Named
@RequestScoped
public class ControleurStatsCaParCategorieProduit
{
    @Inject BalCategorieProduit bal;
    @Inject DaoCategorieProduit dao;
    
    private PieChartModel camembert;
    
    @PostConstruct
    public void init()
    {
        camembert = new PieChartModel();
        
        for(CategorieProduit categ : dao.getToutesLesCategoriesProduit())
        {
            camembert.set(categ.getNomCateg(), bal.caAnnuelCategorieProduit(categ, UtilDate.anneeCourante()));
        }
    }
    
    public PieChartModel getCamembert()
    {
        return camembert;
    }
}
