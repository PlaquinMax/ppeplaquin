package work.controleurs;

import bal.commande.BalCommande;
import bal.lignedecommande.BalLigneDeCommande;
import dao.commande.DaoCommande;
import entites.Commande;
import entites.LigneDeCommande;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class ContEx1 implements Serializable {

    @Inject
    private DaoCommande daoCommande;
    @Inject
    private BalCommande balCommande;
    @Inject
    private BalLigneDeCommande balLigneDeCommande;

    private Commande commande;
    private Long numcomRecherche;
    private Float montantCommandeHT;
    private Float montantCommandeTTC;
    private List<LigneDeCommande> lesLignesDeCommande;

    public Float caLigneDeCommandeHt(LigneDeCommande lgc)
    {
        return balLigneDeCommande.caLigneDeCommandeHT(lgc);
    }

    public Float prixUnitaireTranche(LigneDeCommande lgc)
    {
        return balLigneDeCommande.prixUnitaireTranche(lgc);
    }

    public Float caLigneDeCommandeTtc(LigneDeCommande lgc)
    {
        return balLigneDeCommande.caLigneDeCommandeTTC(lgc);
    }

    public void ecouteurRecherche()
    {
        if (numcomRecherche != null)
        {
            if (daoCommande.getCommande(numcomRecherche) != null)
            {
                commande = daoCommande.getCommande(numcomRecherche);
                montantCommandeHT = balCommande.montantCommandeHT(commande);
                montantCommandeTTC = balCommande.montantCommandeTTC(commande);
                lesLignesDeCommande = commande.getLesLignesDeCommande();
            }
            else
            {
                commande = null;
                montantCommandeHT = null;
                montantCommandeTTC = null;
                FacesContext contexte=FacesContext.getCurrentInstance();
                contexte.addMessage(null,
                                    new FacesMessage(FacesMessage.SEVERITY_INFO, 
                                    null, "PAS DE COMMANDE CONNUE AVEC CE NUMERO."));
            }
        }
    }
    
    //<editor-fold defaultstate="collapsed" desc="GETTERS / SETTERS">
    
    public List<LigneDeCommande> getLesLignesDeCommande() {
        return lesLignesDeCommande;
    }
    
    public void setLesLignesDeCommande(List<LigneDeCommande> lesLignesDeCommande) {
        this.lesLignesDeCommande = lesLignesDeCommande;
    }
    
    public Commande getCommande() {
        return commande;
    }
    
    public void setCommande(Commande commande) {
        this.commande = commande;
    }
    
    public Long getNumcomRecherche() {
        return numcomRecherche;
    }
    
    public void setNumcomRecherche(Long numcomRecherche) {
        this.numcomRecherche = numcomRecherche;
    }
    
    public Float getMontantCommandeHT() {
        return montantCommandeHT;
    }
    
    public void setMontantCommandeHT(Float montantCommandeHT) {
        this.montantCommandeHT = montantCommandeHT;
    }
    
    public Float getMontantCommandeTTC() {
        return montantCommandeTTC;
    }
    
    public void setMontantCommandeTTC(Float montantCommandeTTC) {
        this.montantCommandeTTC = montantCommandeTTC;
    }
//</editor-fold>
}
