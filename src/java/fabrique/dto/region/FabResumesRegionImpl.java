package fabrique.dto.region;

import bal.region.BalRegion;
import dao.region.DaoRegion;
import dto.region.ResumeRegion;
import entites.Region;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Théo
 */
public class FabResumesRegionImpl implements FabResumesRegion, Serializable
{
    @Inject BalRegion   balRegion;
    @Inject DaoRegion   daoRegion;
    
    private ResumeRegion creerResumeRegion(Region region)
    {
        ResumeRegion rr = new ResumeRegion();
        
        rr.setCodeRegion(region.getCodeRegion());
        rr.setNomRegion(region.getNomRegion());
        rr.setNbClients(balRegion.nbClients(region));
        rr.setCaAnnuel(balRegion.caAnneeEnCours(region));
        
        return rr;
    }
    
    @Override
    public ResumeRegion getResumeRegion(String pCodeRegion)
    {
        return creerResumeRegion(daoRegion.getRegion(pCodeRegion));
    }

    @Override
    public List<ResumeRegion> getLesResumesRegion(List<Region> listeRegion)
    {
        List<ResumeRegion> lrr = new LinkedList();
        
        for(Region r : listeRegion)
        {
            lrr.add(creerResumeRegion(r));
        }
        
        return lrr;
    }

    @Override
    public List<ResumeRegion> getLesResumesRegion()
    {
        return getLesResumesRegion(daoRegion.getToutesLesRegions());
    }
    
}
