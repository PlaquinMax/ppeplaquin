package fabrique.dto.region;

import dto.region.ResumeRegion;
import entites.Region;
import java.util.List;

/**
 *
 * @author Théo
 */
public interface FabResumesRegion
{
    ResumeRegion getResumeRegion(String pCodeRegion);
    List<ResumeRegion> getLesResumesRegion(List<Region> listeRegion);
    List<ResumeRegion> getLesResumesRegion();
}
