package fabrique.dto.lignedecommande;

import bal.lignedecommande.BalLigneDeCommande;
import dto.lignedecommande.ResumeLigneDeCommande;
import entites.LigneDeCommande;
import entites.Produit;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Théo
 */
public class FabResumesLigneDeCommandeImpl implements FabResumesLigneDeCommande, Serializable
{
    @Inject BalLigneDeCommande balLgdc;
    
    private ResumeLigneDeCommande creerResumeLigneDeCommande(LigneDeCommande pLgdc)
    {
        ResumeLigneDeCommande rlgdc = new ResumeLigneDeCommande();
        Produit p = pLgdc.getLeProduit();
        
        rlgdc.setRefProd(p.getRefProd());
        rlgdc.setDesigProd(p.getDesigProd());
        rlgdc.setPrixProd(p.getPrixProd());
        rlgdc.setQteCom(pLgdc.getQteCom());
        rlgdc.setMontantHT(balLgdc.caLigneDeCommandeHT(pLgdc));
        rlgdc.setMontantTTC(balLgdc.caLigneDeCommandeTTC(pLgdc));
        
        return rlgdc;
    }
    
    @Override
    public List<ResumeLigneDeCommande> getResumesLigneDeCommande(List<LigneDeCommande> lgdc)
    {
        List<ResumeLigneDeCommande> rl = new LinkedList();
        
        for(LigneDeCommande l : lgdc)
        {
            rl.add(creerResumeLigneDeCommande(l));
        }
        
        return rl;
    }
}
