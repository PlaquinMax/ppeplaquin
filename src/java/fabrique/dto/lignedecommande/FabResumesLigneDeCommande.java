package fabrique.dto.lignedecommande;

import dto.lignedecommande.ResumeLigneDeCommande;
import entites.LigneDeCommande;
import java.util.List;

/**
 *
 * @author Théo
 */
public interface FabResumesLigneDeCommande
{
    List<ResumeLigneDeCommande> getResumesLigneDeCommande(List<LigneDeCommande> lgdc);
}
