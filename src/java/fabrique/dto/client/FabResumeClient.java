package fabrique.dto.client;

import dto.client.ResumeClient;
import entites.Client;
import java.util.List;

/**
 *
 * @author Théo
 */
public interface FabResumeClient
{
    ResumeClient getResumeClient(Long pNumCli);
    List<ResumeClient> getLesResumesClient(List<Client> desClients);
    List<ResumeClient> getLesResumesClient();
}
 