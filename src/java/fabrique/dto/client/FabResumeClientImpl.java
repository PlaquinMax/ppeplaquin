package fabrique.dto.client;

import dao.client.DaoClient;
import dto.client.ResumeClient;
import entites.Client;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Théo
 */
public class FabResumeClientImpl implements FabResumeClient, Serializable
{
    @Inject DaoClient daoClient;
    
    private ResumeClient creerResumeClient(Client client)
    {
        ResumeClient rc = new ResumeClient();
        
        rc.setNumClient(client.getNumCli());
        rc.setNomClient(client.getNomCli());
        rc.setAdrClient(client.getAdrCli());
        rc.setEmail(client.getEmail());
        rc.setPassword(client.getPassword());
        rc.setLesCommandes(client.getLesCommandes());
        
        return rc;
    }
    
    @Override
    public ResumeClient getResumeClient(Long pNumCli)
    {
        return creerResumeClient(daoClient.getLeClient(pNumCli));
    }

    @Override
    public List<ResumeClient> getLesResumesClient(List<Client> desClients)
    {
        List<ResumeClient> lrc = new LinkedList();
        
        for(Client c : desClients)
        {
            lrc.add(creerResumeClient(c));
        }
        
        return lrc;
    }

    @Override
    public List<ResumeClient> getLesResumesClient()
    {
        return getLesResumesClient(daoClient.getTousLesClients());
    }
}
