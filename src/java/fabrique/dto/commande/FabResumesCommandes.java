package fabrique.dto.commande;

import dto.commande.ResumeCommande;
import entites.Commande;
import java.util.List;

/**
 *
 * @author Théo
 */
public interface FabResumesCommandes
{
    ResumeCommande getResumeCommande(Long pNumCom);
    List<ResumeCommande> getLesResumesCommande(List<Commande> desCommandes);
    List<ResumeCommande> getLesResumesCommande();
}
