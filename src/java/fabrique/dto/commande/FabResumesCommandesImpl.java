package fabrique.dto.commande;

import bal.commande.BalCommande;
import dao.commande.DaoCommande;
import dto.commande.ResumeCommande;
import entites.Commande;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;

/**
 *
 * @author Théo
 */
public class FabResumesCommandesImpl implements FabResumesCommandes, Serializable
{
    @Inject DaoCommande daoCommande;
    @Inject BalCommande balCommande;
    
    private ResumeCommande creerResumeCommande(Commande cmd)
    {
        ResumeCommande rc = new ResumeCommande();
        
        rc.setDatecom(cmd.getDateCom());
        rc.setEtatcom(cmd.getEtatCom());
        rc.setMontantHT(balCommande.montantCommandeHT(cmd));
        rc.setMontantTTC(balCommande.montantCommandeTTC(cmd));
        rc.setNumcom(cmd.getNumCom());
        
        return rc;
    }

    @Override
    public ResumeCommande getResumeCommande(Long pNumCom)
    {
        return creerResumeCommande(daoCommande.getCommande(pNumCom));
    }
    
    @Override
    public List<ResumeCommande> getLesResumesCommande(List<Commande> desCommandes)
    {
        List<ResumeCommande> lc = new LinkedList();
        
        for(Commande c : desCommandes)
        {
            lc.add(creerResumeCommande(c));
        }
        
        return lc;
    }

    @Override
    public List<ResumeCommande> getLesResumesCommande()
    {
        return getLesResumesCommande(daoCommande.getToutesLesCommandes());
    }
}
